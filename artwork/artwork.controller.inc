class ArtworkController extends DrupalDefaultEntityController {
    
    function artwork_load($aid = NULL, $vid = NULL, $reset = FALSE) {
        $aids = (isset($aid) ? array($aid) : array());
        $conditions = (isset($vid) ? array('vid' => $vid) : array());
        $artwork = artwork_load_multiple($aids, $conditions, $reset);
        return $artwork ? reset($artwork) : FALSE;
    }

    function artwork_load_multiple($aids = array(), $conditions = array(), $reset = FALSE) {
        return entity_load('artwork', $aids, $conditions, $reset);
    }

    function artwork_menu() {
        $items['admin/structure/artworks'] = array(
            'title' => 'Manage artworks',
            'description' => 'Manage artworks.',
            'page callback' => 'artwork_overview_types',
            'access arguments' => array('administer artworks'),
        );
        $items['admin/structure/artworks/manage/%artwork_type'] = array(
            'title' => 'View artwork type',
            'title callback' => 'artwork_type_page_title',
            'title arguments' => array(4),
            'page callback' => 'artwork_information',
            'page arguments' => array(4),
            'access arguments' => array('administer artworks'),
        );
        $items['admin/structure/artworks/manage/%artwork_type/view'] =
        array(
            'title' => 'View',
            'type' => MENU_DEFAULT_LOCAL_TASK,
        );
        return $items;
    }

    function artwork_overview_types() {
        foreach (artwork_types() as $type => $info) {
            $type_url_str = str_replace('_', '-', $type);
            $label = t('View @type', array('@type' => $info->name));
            $items[] = l($label, 'admin/structure/artworks/manage/' .
            $type_url_str);
        }
        return theme('item_list', array('items' => $items));
    }

    function artwork_type_page_title($type) {
        return t('Manage @type', array('@type' => $type->name));
    }

    function artwork_information($artwork_type) {
        return $artwork_type->name . ': ' . $artwork_type->description;
    }
}